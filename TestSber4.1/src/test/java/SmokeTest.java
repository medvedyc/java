/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static java.nio.file.StandardOpenOption.*;
import java.nio.file.*;
import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author medve
 */
public class SmokeTest {

    public SmokeTest() {
    }
    private static ArrayList<String> TextArray = new ArrayList<String>();

    @BeforeClass
    public static void setUpClass() {
        String TestFilePath = "DataFiles\\test.txt";
        Path PathFile = Paths.get(TestFilePath);

        try (InputStream in = Files.newInputStream(PathFile);
                BufferedReader reader
                = new BufferedReader(new InputStreamReader(in))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                TextArray.add(line);
            }
        } catch (IOException x) {
            System.err.println(x);
        }
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void SmokeTestLine1() {
        String line = TextArray.get(0);
        String lineArray[] = line.split(";");
        boolean actual = false;
        int operand1 = Integer.parseInt(lineArray[0]);
        int operand2 = Integer.parseInt(lineArray[1]);
        String operation = lineArray[2];
        int result = Integer.parseInt(lineArray[3]);
        int realResult = 0;
        switch (operation) {
            case "+":
                realResult = operand1 + operand2;
                break;
            case "-":
                realResult = operand1 - operand2;
                break;
            case "/":
                realResult = operand1 / operand2;
                break;
            case "*":
                realResult = operand1 * operand2;
                break;
        }
        if(realResult == result)
        {
            actual = true;
        }
        assertTrue(actual);
    }
    @Test
    public void SmokeTestLine2() {
        String line = TextArray.get(1);
        String lineArray[] = line.split(";");
        boolean actual = false;
        int operand1 = Integer.parseInt(lineArray[0]);
        int operand2 = Integer.parseInt(lineArray[1]);
        String operation = lineArray[2];
        int result = Integer.parseInt(lineArray[3]);
        int realResult = 0;
        switch (operation) {
            case "+":
                realResult = operand1 + operand2;
                break;
            case "-":
                realResult = operand1 - operand2;
                break;
            case "/":
                realResult = operand1 / operand2;
                break;
            case "*":
                realResult = operand1 * operand2;
                break;
        }
        if(realResult == result)
        {
            actual = true;
        }
        assertTrue(actual);
    }
    @Test
    public void SmokeTestLine3() {
        String line = TextArray.get(2);
        String lineArray[] = line.split(";");
        boolean actual = false;
        int operand1 = Integer.parseInt(lineArray[0]);
        int operand2 = Integer.parseInt(lineArray[1]);
        String operation = lineArray[2];
        int result = Integer.parseInt(lineArray[3]);
        int realResult = 0;
        switch (operation) {
            case "+":
                realResult = operand1 + operand2;
                break;
            case "-":
                realResult = operand1 - operand2;
                break;
            case "/":
                realResult = operand1 / operand2;
                break;
            case "*":
                realResult = operand1 * operand2;
                break;
        }
        if(realResult == result)
        {
            actual = true;
        }
        assertTrue(actual);
    }
    @Test
    public void SmokeTestLine4() {
        String line = TextArray.get(3);
        String lineArray[] = line.split(";");
        boolean actual = false;
        int operand1 = Integer.parseInt(lineArray[0]);
        int operand2 = Integer.parseInt(lineArray[1]);
        String operation = lineArray[2];
        int result = Integer.parseInt(lineArray[3]);
        int realResult = 0;
        switch (operation) {
            case "+":
                realResult = operand1 + operand2;
                break;
            case "-":
                realResult = operand1 - operand2;
                break;
            case "/":
                realResult = operand1 / operand2;
                break;
            case "*":
                realResult = operand1 * operand2;
                break;
        }
        if(realResult == result)
        {
            actual = true;
        }
        assertTrue(actual);
    }
}
