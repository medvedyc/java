/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import static java.nio.file.StandardOpenOption.*;
import java.nio.file.*;
import java.io.*;
import java.util.ArrayList;

/**
 *
 * @author medve
 */
public class TestNoEmptyAndValidMeanings {

    private static ArrayList<String> TextArray = new ArrayList<String>();

    public TestNoEmptyAndValidMeanings() {
    }

    @BeforeClass
    public static void setUpClass() {
        String TestFilePath = "DataFiles\\test.txt";
        Path PathFile = Paths.get(TestFilePath);

        try (InputStream in = Files.newInputStream(PathFile);
                BufferedReader reader
                = new BufferedReader(new InputStreamReader(in))) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                TextArray.add(line);
            }
        } catch (IOException x) {
            System.err.println(x);
        }

    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void TestNoEptyOperand1InLine1() {
        String line1 = TextArray.get(0);
        String lineArray[] = line1.split(";");
        boolean actual;
        actual = lineArray[0].isEmpty();
        assertFalse(actual);
    }

    @Test
    public void TestNoEptyOperand2InLine1() {
        String line1 = TextArray.get(0);
        String lineArray[] = line1.split(";");
        boolean actual;
        actual = lineArray[1].isEmpty();
        assertFalse(actual);
    }

    @Test
    public void TestNoEptyOperationInLine1() {
        String line1 = TextArray.get(0);
        String lineArray[] = line1.split(";");
        boolean actual;
        actual = lineArray[2].isEmpty();
        assertFalse(actual);
    }

    @Test
    public void TestNoEptyResaltInLine1() {
        String line1 = TextArray.get(0);
        String lineArray[] = line1.split(";");
        boolean actual;
        actual = lineArray[3].isEmpty();
        assertFalse(actual);
    }

    @Test
    public void TestNoEptyOperand1InLine2() {
        String line2 = TextArray.get(1);
        String lineArray[] = line2.split(";");
        boolean actual;
        actual = lineArray[0].isEmpty();
        assertFalse(actual);
    }

    @Test
    public void TestNoEptyOperand2InLine2() {
        String line2 = TextArray.get(1);
        String lineArray[] = line2.split(";");
        boolean actual;
        actual = lineArray[1].isEmpty();
        assertFalse(actual);
    }

    @Test
    public void TestNoEptyOperationInLine2() {
        String line2 = TextArray.get(1);
        String lineArray[] = line2.split(";");
        boolean actual;
        actual = lineArray[2].isEmpty();
        assertFalse(actual);
    }

    @Test
    public void TestNoEptyResaltInLine2() {
        String line2 = TextArray.get(1);
        String lineArray[] = line2.split(";");
        boolean actual;
        actual = lineArray[3].isEmpty();
        assertFalse(actual);
    }
    @Test
    public void TestNoEptyOperand1InLine3() {
        String line3 = TextArray.get(2);
        String lineArray[] = line3.split(";");
         boolean actual;
        actual = lineArray[0].isEmpty();
        assertFalse(actual);
    }
    @Test
    public void TestNoEptyOperand2InLine3() {
        String line3 = TextArray.get(2);
        String lineArray[] = line3.split(";");
         boolean actual;
        actual = lineArray[1].isEmpty();
        assertFalse(actual);
    }
    @Test
    public void TestNoEptyOperationInLine3() {
        String line3 = TextArray.get(2);
        String lineArray[] = line3.split(";");
         boolean actual;
        actual = lineArray[2].isEmpty();
        assertFalse(actual);
    }
    @Test
    public void TestNoEptyResaltInLine3() {
        String line3 = TextArray.get(2);
        String lineArray[] = line3.split(";");
         boolean actual;
        actual = lineArray[3].isEmpty();
        assertFalse(actual);
    }
@Test
    public void TestNoEptyOperand1InLine4() {
        String line4 = TextArray.get(3);
        String lineArray[] = line4.split(";");
         boolean actual;
        actual = lineArray[0].isEmpty();
        assertFalse(actual);
    }
    @Test
    public void TestNoEptyOperand2InLine4() {
        String line4 = TextArray.get(3);
        String lineArray[] = line4.split(";");
         boolean actual;
        actual = lineArray[1].isEmpty();
        assertFalse(actual);
    }
    @Test
    public void TestNoEptyOperationInLine4() {
        String line4 = TextArray.get(3);
        String lineArray[] = line4.split(";");
         boolean actual;
        actual = lineArray[2].isEmpty();
        assertFalse(actual);
    }
    @Test
    public void TestNoEptyResaltInLine4() {
        String line4 = TextArray.get(3);
        String lineArray[] = line4.split(";");
         boolean actual;
        actual = lineArray[3].isEmpty();
        assertFalse(actual);
    }
    @Test
    public void TestValidOperand1InLine1() {
        String line = TextArray.get(0);
        String lineArray[] = line.split(";");
        boolean actual;
         try {
            Integer.parseInt(lineArray[0]);
            actual = true;
        } catch (NumberFormatException e) {
            actual = false;
        }
        assertTrue(actual);
    }

    @Test
    public void TestValidOperand2InLine1() {
        String line = TextArray.get(0);
        String lineArray[] = line.split(";");
        boolean actual;
        try {
            Integer.parseInt(lineArray[1]);
            actual = true;
        } catch (NumberFormatException e) {
            actual = false;
        }
        assertTrue(actual);
    }

    @Test
    public void TestValidOperationInLine1() {
        String line = TextArray.get(0);
        String lineArray[] = line.split(";");
        boolean actual = false;
        if (lineArray[2].equals("+")
                || lineArray[2].equals("-")
                || lineArray[2].equals("/")
                || lineArray[2].equals("*")) {
            actual = true;
        }
        assertTrue(actual);
    }

    @Test
    public void TestValidResultInLine1() {
        String line = TextArray.get(0);
        String lineArray[] = line.split(";");
        boolean actual;
         try {
            Double.parseDouble(lineArray[3]);
            actual = true;
        } catch (NumberFormatException e) {
            actual = false;
        }
        assertTrue(actual);
    }
    @Test
    public void TestValidOperand1InLine2() {
        String line = TextArray.get(1);
        String lineArray[] = line.split(";");
        boolean actual;
         try {
            Integer.parseInt(lineArray[0]);
            actual = true;
        } catch (NumberFormatException e) {
            actual = false;
        }
        assertTrue(actual);
    }

    @Test
    public void TestValidOperand2InLine2() {
        String line = TextArray.get(1);
        String lineArray[] = line.split(";");
        boolean actual;
        try {
            Integer.parseInt(lineArray[1]);
            actual = true;
        } catch (NumberFormatException e) {
            actual = false;
        }
        assertTrue(actual);
    }

    @Test
    public void TestValidOperationInLine2() {
        String line = TextArray.get(1);
        String lineArray[] = line.split(";");
        boolean actual = false;
        if (lineArray[2].equals("+")
                || lineArray[2].equals("-")
                || lineArray[2].equals("/")
                || lineArray[2].equals("*")) {
            actual = true;
        }
        assertTrue(actual);
    }

    @Test
    public void TestValidResultInLine2() {
        String line = TextArray.get(1);
        String lineArray[] = line.split(";");
        boolean actual;
         try {
            Double.parseDouble(lineArray[3]);
            actual = true;
        } catch (NumberFormatException e) {
            actual = false;
        }
        assertTrue(actual);
    }
    @Test
    public void TestValidOperand1InLine3() {
        String line = TextArray.get(2);
        String lineArray[] = line.split(";");
        boolean actual;
         try {
            Integer.parseInt(lineArray[0]);
            actual = true;
        } catch (NumberFormatException e) {
            actual = false;
        }
        assertTrue(actual);
    }

    @Test
    public void TestValidOperand2InLine3() {
        String line = TextArray.get(2);
        String lineArray[] = line.split(";");
        boolean actual;
        try {
            Integer.parseInt(lineArray[1]);
            actual = true;
        } catch (NumberFormatException e) {
            actual = false;
        }
        assertTrue(actual);
    }

    @Test
    public void TestValidOperationInLine3() {
        String line = TextArray.get(2);
        String lineArray[] = line.split(";");
        boolean actual = false;
        if (lineArray[2].equals("+")
                || lineArray[2].equals("-")
                || lineArray[2].equals("/")
                || lineArray[2].equals("*")) {
            actual = true;
        }
        assertTrue(actual);
    }

    @Test
    public void TestValidResultInLine3() {
        String line = TextArray.get(2);
        String lineArray[] = line.split(";");
        boolean actual;
         try {
            Double.parseDouble(lineArray[3]);
            actual = true;
        } catch (NumberFormatException e) {
            actual = false;
        }
        assertTrue(actual);
    }
    @Test
    public void TestValidOperand1InLine4() {
        String line = TextArray.get(3);
        String lineArray[] = line.split(";");
        boolean actual;
         try {
            Integer.parseInt(lineArray[0]);
            actual = true;
        } catch (NumberFormatException e) {
            actual = false;
        }
        assertTrue(actual);
    }

    @Test
    public void TestValidOperand2InLine4() {
        String line = TextArray.get(3);
        String lineArray[] = line.split(";");
        boolean actual;
        try {
            Integer.parseInt(lineArray[1]);
            actual = true;
        } catch (NumberFormatException e) {
            actual = false;
        }
        assertTrue(actual);
    }

    @Test
    public void TestValidOperationInLine4() {
        String line = TextArray.get(3);
        String lineArray[] = line.split(";");
        boolean actual = false;
        if (lineArray[2].equals("+")
                || lineArray[2].equals("-")
                || lineArray[2].equals("/")
                || lineArray[2].equals("*")) {
            actual = true;
        }
        assertTrue(actual);
    }

    @Test
    public void TestValidResultInLine4() {
        String line = TextArray.get(3);
        String lineArray[] = line.split(";");
        boolean actual;
         try {
            Double.parseDouble(lineArray[3]);
            actual = true;
        } catch (NumberFormatException e) {
            actual = false;
        }
        assertTrue(actual);
    }
}
